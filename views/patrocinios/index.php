<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Patrocinios';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="patrocinios-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Patrocinios', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_patrocinio',
            'codigo_patrocinador',
            'matricula_embarcacion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
