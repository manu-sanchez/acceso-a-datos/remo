<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Models\Patrocinios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="patrocinios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_patrocinador')->textInput() ?>

    <?= $form->field($model, 'matricula_embarcacion')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
