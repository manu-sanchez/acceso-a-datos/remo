<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Models\Patrocinios */

$this->title = 'Update Patrocinios: ' . $model->id_patrocinio;
$this->params['breadcrumbs'][] = ['label' => 'Patrocinios', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_patrocinio, 'url' => ['view', 'id' => $model->id_patrocinio]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="patrocinios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
