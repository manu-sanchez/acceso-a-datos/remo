<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Models\PermisosConduccion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="permisos-conduccion-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'dni_entrenador')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'permiso')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
