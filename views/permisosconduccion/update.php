<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Models\PermisosConduccion */

$this->title = 'Update Permisos Conduccion: ' . $model->id_permiso;
$this->params['breadcrumbs'][] = ['label' => 'Permisos Conduccions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_permiso, 'url' => ['view', 'id' => $model->id_permiso]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="permisos-conduccion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
