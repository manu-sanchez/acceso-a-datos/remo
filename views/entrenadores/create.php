<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Models\Entrenadores */

$this->title = 'Create Entrenadores';
$this->params['breadcrumbs'][] = ['label' => 'Entrenadores', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entrenadores-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
