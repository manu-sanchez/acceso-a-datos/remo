<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Embarcaciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="embarcaciones-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Embarcaciones', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'matricula',
            'nombre_tecnico',
            'mote',
            'fabricante',
            'num_tripulantes',
            //'necesita_patron',
            //'eslora',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
