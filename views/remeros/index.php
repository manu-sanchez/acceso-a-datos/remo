<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Remeros';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="remeros-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Remeros', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_remero',
            'codigo_categoria',
            'codigo_remero',
            'dni',
            'nombre_completo',
            //'fecha_nac',
            //'codigo_patrocinador',
            //'lesiones',
            //'anios_exp',
            //'datos_padre',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
