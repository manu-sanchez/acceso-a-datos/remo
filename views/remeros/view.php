<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\Models\Remeros */

$this->title = $model->id_remero;
$this->params['breadcrumbs'][] = ['label' => 'Remeros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="remeros-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_remero], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_remero], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_remero',
            'codigo_categoria',
            'codigo_remero',
            'dni',
            'nombre_completo',
            'fecha_nac',
            'codigo_patrocinador',
            'lesiones',
            'anios_exp',
            'datos_padre',
        ],
    ]) ?>

</div>
