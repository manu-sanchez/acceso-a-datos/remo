<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Models\Remeros */

$this->title = 'Update Remeros: ' . $model->id_remero;
$this->params['breadcrumbs'][] = ['label' => 'Remeros', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_remero, 'url' => ['view', 'id' => $model->id_remero]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="remeros-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
