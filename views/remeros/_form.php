<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Models\Remeros */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="remeros-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'codigo_categoria')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'codigo_remero')->textInput() ?>

    <?= $form->field($model, 'dni')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre_completo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fecha_nac')->textInput() ?>

    <?= $form->field($model, 'codigo_patrocinador')->textInput() ?>

    <?= $form->field($model, 'lesiones')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'anios_exp')->textInput() ?>

    <?= $form->field($model, 'datos_padre')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
