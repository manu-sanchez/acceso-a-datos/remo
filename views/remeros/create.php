<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Models\Remeros */

$this->title = 'Create Remeros';
$this->params['breadcrumbs'][] = ['label' => 'Remeros', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="remeros-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
