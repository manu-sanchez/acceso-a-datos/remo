<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\Models\FechasUso */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="fechas-uso-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_uso')->textInput() ?>

    <?= $form->field($model, 'fecha_uso')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
