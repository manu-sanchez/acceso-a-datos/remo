<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Fechas Usos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fechas-uso-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Fechas Uso', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_fecha',
            'id_uso',
            'fecha_uso',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
