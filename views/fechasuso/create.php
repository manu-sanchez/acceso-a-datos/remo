<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Models\FechasUso */

$this->title = 'Create Fechas Uso';
$this->params['breadcrumbs'][] = ['label' => 'Fechas Usos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="fechas-uso-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
