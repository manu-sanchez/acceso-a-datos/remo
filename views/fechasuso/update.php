<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Models\FechasUso */

$this->title = 'Update Fechas Uso: ' . $model->id_fecha;
$this->params['breadcrumbs'][] = ['label' => 'Fechas Usos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_fecha, 'url' => ['view', 'id' => $model->id_fecha]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="fechas-uso-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
