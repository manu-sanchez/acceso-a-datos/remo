<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Models\Directivos */

$this->title = 'Create Directivos';
$this->params['breadcrumbs'][] = ['label' => 'Directivos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="directivos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
