<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Models\JuegosRemos */

$this->title = 'Create Juegos Remos';
$this->params['breadcrumbs'][] = ['label' => 'Juegos Remos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="juegos-remos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
