<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Juegos Remos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="juegos-remos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Juegos Remos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'codigo',
            'fabricante',
            'num_remos',
            'dureza',
            'material',
            //'matricula_embarcacion',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
