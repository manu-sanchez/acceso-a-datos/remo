<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Models\JuegosRemos */

$this->title = 'Update Juegos Remos: ' . $model->codigo;
$this->params['breadcrumbs'][] = ['label' => 'Juegos Remos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->codigo, 'url' => ['view', 'id' => $model->codigo]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="juegos-remos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
