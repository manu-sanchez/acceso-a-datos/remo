<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Models\UsosEmbarcaciones */

$this->title = 'Create Usos Embarcaciones';
$this->params['breadcrumbs'][] = ['label' => 'Usos Embarcaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usos-embarcaciones-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
