<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Usos Embarcaciones';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="usos-embarcaciones-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Usos Embarcaciones', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_uso',
            'matricula_embarcacion',
            'id_remero',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
