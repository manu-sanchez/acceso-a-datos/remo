<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\Models\UsosEmbarcaciones */

$this->title = 'Update Usos Embarcaciones: ' . $model->id_uso;
$this->params['breadcrumbs'][] = ['label' => 'Usos Embarcaciones', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_uso, 'url' => ['view', 'id' => $model->id_uso]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="usos-embarcaciones-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
