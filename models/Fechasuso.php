<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "fechas_uso".
 *
 * @property int $id_fecha
 * @property int|null $id_uso
 * @property string|null $fecha_uso
 *
 * @property UsosEmbarcaciones $uso
 */
class Fechasuso extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'fechas_uso';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_uso'], 'integer'],
            [['fecha_uso'], 'safe'],
            [['id_uso', 'fecha_uso'], 'unique', 'targetAttribute' => ['id_uso', 'fecha_uso']],
            [['id_uso'], 'exist', 'skipOnError' => true, 'targetClass' => UsosEmbarcaciones::className(), 'targetAttribute' => ['id_uso' => 'id_uso']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_fecha' => 'Id Fecha',
            'id_uso' => 'Id Uso',
            'fecha_uso' => 'Fecha Uso',
        ];
    }

    /**
     * Gets query for [[Uso]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUso()
    {
        return $this->hasOne(UsosEmbarcaciones::className(), ['id_uso' => 'id_uso']);
    }
}
