<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "patrocinios".
 *
 * @property int $id_patrocinio
 * @property int|null $codigo_patrocinador
 * @property string|null $matricula_embarcacion
 *
 * @property Embarcaciones $matriculaEmbarcacion
 * @property Patrocinadores $codigoPatrocinador
 */
class Patrocinios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'patrocinios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_patrocinador'], 'integer'],
            [['matricula_embarcacion'], 'string', 'max' => 6],
            [['codigo_patrocinador', 'matricula_embarcacion'], 'unique', 'targetAttribute' => ['codigo_patrocinador', 'matricula_embarcacion']],
            [['matricula_embarcacion'], 'exist', 'skipOnError' => true, 'targetClass' => Embarcaciones::className(), 'targetAttribute' => ['matricula_embarcacion' => 'matricula']],
            [['codigo_patrocinador'], 'exist', 'skipOnError' => true, 'targetClass' => Patrocinadores::className(), 'targetAttribute' => ['codigo_patrocinador' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_patrocinio' => 'Id Patrocinio',
            'codigo_patrocinador' => 'Codigo Patrocinador',
            'matricula_embarcacion' => 'Matricula Embarcacion',
        ];
    }

    /**
     * Gets query for [[MatriculaEmbarcacion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMatriculaEmbarcacion()
    {
        return $this->hasOne(Embarcaciones::className(), ['matricula' => 'matricula_embarcacion']);
    }

    /**
     * Gets query for [[CodigoPatrocinador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoPatrocinador()
    {
        return $this->hasOne(Patrocinadores::className(), ['codigo' => 'codigo_patrocinador']);
    }
}
