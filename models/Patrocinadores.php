<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "patrocinadores".
 *
 * @property int $codigo
 * @property string $nombre
 * @property float|null $cantidad_aportada
 * @property string|null $dni_directivo
 *
 * @property Directivos $dniDirectivo
 * @property Patrocinios[] $patrocinios
 * @property Embarcaciones[] $matriculaEmbarcacions
 * @property Remeros[] $remeros
 */
class Patrocinadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'patrocinadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['cantidad_aportada'], 'number'],
            [['nombre'], 'string', 'max' => 30],
            [['dni_directivo'], 'string', 'max' => 9],
            [['dni_directivo'], 'exist', 'skipOnError' => true, 'targetClass' => Directivos::className(), 'targetAttribute' => ['dni_directivo' => 'dni']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'cantidad_aportada' => 'Cantidad Aportada',
            'dni_directivo' => 'Dni Directivo',
        ];
    }

    /**
     * Gets query for [[DniDirectivo]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniDirectivo()
    {
        return $this->hasOne(Directivos::className(), ['dni' => 'dni_directivo']);
    }

    /**
     * Gets query for [[Patrocinios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPatrocinios()
    {
        return $this->hasMany(Patrocinios::className(), ['codigo_patrocinador' => 'codigo']);
    }

    /**
     * Gets query for [[MatriculaEmbarcacions]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMatriculaEmbarcacions()
    {
        return $this->hasMany(Embarcaciones::className(), ['matricula' => 'matricula_embarcacion'])->viaTable('patrocinios', ['codigo_patrocinador' => 'codigo']);
    }

    /**
     * Gets query for [[Remeros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRemeros()
    {
        return $this->hasMany(Remeros::className(), ['codigo_patrocinador' => 'codigo']);
    }
}
