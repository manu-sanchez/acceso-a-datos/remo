<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "categorias".
 *
 * @property string $codigo
 * @property string $nombre
 * @property string $rango_edad
 * @property int|null $num_remeros
 *
 * @property Entrenadores[] $entrenadores
 * @property Remeros[] $remeros
 */
class Categorias extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'categorias';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo', 'nombre', 'rango_edad'], 'required'],
            [['num_remeros'], 'integer'],
            [['codigo'], 'string', 'max' => 2],
            [['nombre'], 'string', 'max' => 15],
            [['rango_edad'], 'string', 'max' => 5],
            [['codigo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'rango_edad' => 'Rango Edad',
            'num_remeros' => 'Num Remeros',
        ];
    }

    /**
     * Gets query for [[Entrenadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntrenadores()
    {
        return $this->hasMany(Entrenadores::className(), ['codigo_categoria' => 'codigo']);
    }

    /**
     * Gets query for [[Remeros]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRemeros()
    {
        return $this->hasMany(Remeros::className(), ['codigo_categoria' => 'codigo']);
    }
}
