<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "permisos_conduccion".
 *
 * @property int $id_permiso
 * @property string|null $dni_entrenador
 * @property string|null $permiso
 *
 * @property Entrenadores $dniEntrenador
 */
class Permisosconduccion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'permisos_conduccion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni_entrenador'], 'string', 'max' => 9],
            [['permiso'], 'string', 'max' => 5],
            [['dni_entrenador', 'permiso'], 'unique', 'targetAttribute' => ['dni_entrenador', 'permiso']],
            [['dni_entrenador'], 'exist', 'skipOnError' => true, 'targetClass' => Entrenadores::className(), 'targetAttribute' => ['dni_entrenador' => 'dni']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_permiso' => 'Id Permiso',
            'dni_entrenador' => 'Dni Entrenador',
            'permiso' => 'Permiso',
        ];
    }

    /**
     * Gets query for [[DniEntrenador]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniEntrenador()
    {
        return $this->hasOne(Entrenadores::className(), ['dni' => 'dni_entrenador']);
    }
}
