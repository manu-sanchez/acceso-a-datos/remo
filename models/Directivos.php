<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "directivos".
 *
 * @property string $dni
 * @property string $nombre_completo
 * @property string $cargo
 *
 * @property Entrenadores[] $entrenadores
 * @property Patrocinadores[] $patrocinadores
 */
class Directivos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'directivos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni', 'nombre_completo', 'cargo'], 'required'],
            [['dni'], 'string', 'max' => 9],
            [['nombre_completo'], 'string', 'max' => 30],
            [['cargo'], 'string', 'max' => 15],
            [['dni'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'nombre_completo' => 'Nombre Completo',
            'cargo' => 'Cargo',
        ];
    }

    /**
     * Gets query for [[Entrenadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntrenadores()
    {
        return $this->hasMany(Entrenadores::className(), ['dni_directivo' => 'dni']);
    }

    /**
     * Gets query for [[Patrocinadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPatrocinadores()
    {
        return $this->hasMany(Patrocinadores::className(), ['dni_directivo' => 'dni']);
    }
}
