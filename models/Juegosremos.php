<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "juegos_remos".
 *
 * @property int $codigo
 * @property string $fabricante
 * @property int|null $num_remos
 * @property string|null $dureza
 * @property string|null $material
 * @property string|null $matricula_embarcacion
 *
 * @property Embarcaciones $matriculaEmbarcacion
 */
class Juegosremos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'juegos_remos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fabricante'], 'required'],
            [['num_remos'], 'integer'],
            [['fabricante'], 'string', 'max' => 15],
            [['dureza'], 'string', 'max' => 10],
            [['material'], 'string', 'max' => 20],
            [['matricula_embarcacion'], 'string', 'max' => 6],
            [['matricula_embarcacion'], 'unique'],
            [['matricula_embarcacion'], 'exist', 'skipOnError' => true, 'targetClass' => Embarcaciones::className(), 'targetAttribute' => ['matricula_embarcacion' => 'matricula']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'fabricante' => 'Fabricante',
            'num_remos' => 'Num Remos',
            'dureza' => 'Dureza',
            'material' => 'Material',
            'matricula_embarcacion' => 'Matricula Embarcacion',
        ];
    }

    /**
     * Gets query for [[MatriculaEmbarcacion]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMatriculaEmbarcacion()
    {
        return $this->hasOne(Embarcaciones::className(), ['matricula' => 'matricula_embarcacion']);
    }
}
